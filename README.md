官网地址：https://ooicat.com （已被墙）

如果打不开，可以用备用地址：

https://ooicat.uk （正常）

https://ooicat.org.uk （正常）

https://ooicat.net （已被墙）

https://ooicat.org （已被墙）

https://ooicat.xyz （已被墙）

客户端下载和订阅链接也可以使用备用地址

例如 订阅链接为：https://ooicat.com/link/xxxxxx

把 https://ooicat.com/link/xxxxxx 修改为 https://ooicat.uk/link/xxxxxx  这个地址就可以正常访问了
